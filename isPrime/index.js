const isPrime = (num) => {
  // do code here
  let isPrime = true;
  if (num === 1) {
    console.log("Bukan neither prime");
  } else if (num > 1) {
    for (let i = 2; i <= num; i++) {
      if (num % i === 0) {
        isPrime = true;
      }
    }
  }
  if (isPrime) {
    return `${num} IS A PRIME`; // true
  } else {
    return `${num} IS NOT A PRIME`;
  }
};

// do not change this code below
const test = (testCase, result) => console.log(testCase === result);

// test(isPrime(22), "22 ?");
test(isPrime(2), "2 IS A PRIME");
test(isPrime(283), "283 IS A PRIME");
test(isPrime(21), "21 IS NOT A PRIME");
test(isPrime(389), "389 IS A PRIME");
test(isPrime(973), "973 IS NOT A PRIME");
