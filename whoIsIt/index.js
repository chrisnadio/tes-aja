const whoIsit = (firstDay, secondDay) => {
  // do code here
  // let isCommon = true;
  let common = [];
  for (let i = 0; i < firstDay.length; i++) {
    for (let n = 0; n < secondDay.length; n++) {
      if (firstDay[i] === secondDay[n]) common.push(firstDay[i]);
    }
  }
  return common;
};

//do not change this code below
const test = (testCase, result) => {
  if (testCase.sort().join() === result.sort().join()) {
    return console.log(true);
  }
  return console.log(false);
};

test(whoIsit(["Joko", "Ani", "Budi"], ["Joko"]), []);
test(
  whoIsit(
    ["Andi", "Prabowo", "Jokowi", "Roberto"],
    ["Sebastian", "Rachel", "Jokowi", "Prabowo"]
  ),
  ["Jokowi", "Prabowo"]
); //true
test(
  whoIsit(
    ["Zoe", "Pearson", "Dona", "Luis"],
    ["Dona", "Robert", "Forstman", "Katrina"]
  ),
  ["Dona"]
); //true
test(whoIsit(["Badu", "Gilang", "Silvy"], ["Amal", "Adrian", "Margi"]), []); //false
test(
  whoIsit(
    ["Ian", "Aron", "Udin", "Adit", "Natalia", "Lia"],
    ["Ian", "Aron", "Udin", "Adit", "Natalia", "Lia"]
  ),
  ["Ian", "Aron", "Udin", "Adit", "Natalia", "Lia"]
); //true
